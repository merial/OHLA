/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testing;

import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Meri Alho
 */
@RunWith(Parameterized.class)
public class TimeToSecTest {

    private String inputString;
    private int expResult;
    
    
    public TimeToSecTest(String inputString, int expResult) {
        this.inputString = inputString;
        this.expResult = expResult;
    }

    
    @Parameterized.Parameters
    public static List testiTapaukset(){
        return Arrays.asList(new Object[][] {
            {"0:00:00", 0},
            {"0:00:01", 1},
            {"8:53:19", 31999},
            {"0:11:06", 666},
            {"2:00:00", 7200}
        });
    }

    @Test
    public void testSecToTime() {
        System.out.print("secToTime test, input: " +  inputString);
        assertEquals(expResult, TimeUtils.timeToSec(inputString));
        System.out.println(" - result: " + TimeUtils.timeToSec(inputString));
    }
}
