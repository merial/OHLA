package testing;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.util.Collection;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Meri Alho
 */

@RunWith(Parameterized.class)
public class SecToTimeTest {
    
    private int inputNumber;
    private String expResult;
    
    
    public SecToTimeTest(int inputNumber, String expResult) {
        this.inputNumber = inputNumber;
        this.expResult = expResult;
    }

    
    @Parameterized.Parameters
    public static List testiTapaukset(){
        return Arrays.asList(new Object[][] {
            {0,"0:00:00"},
            {-6, "-1"},
            {32001, "-1"},
            {1,"0:00:01"},
            {31999, "8:53:19"},
            {666, "0:11:06"},
            {360, "0:06:00"}
        });
    }

    @Test
    public void testSecToTime() {
        System.out.print("secToTime test, inputnumber: " + inputNumber );
        assertEquals(expResult, TimeUtils.secToTime(inputNumber));
        System.out.println(" - result: " + TimeUtils.secToTime(inputNumber));
    }
    
}
